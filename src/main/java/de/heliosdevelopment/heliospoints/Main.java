package de.heliosdevelopment.heliospoints;

import de.heliosdevelopment.heliospoints.command.HeliosPointsCommand;
import de.heliosdevelopment.heliospoints.listener.MessageReciveListener;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import de.heliosdevelopment.heliospoints.listener.PointsListener;
import de.heliosdevelopment.heliospoints.player.PlayerManager;

public class Main extends JavaPlugin {

    private static Main instance;
    private MySQL mysql;
    private DatabaseType databaseType;

    @Override
    public void onEnable() {
        instance = this;
        check("mysql.enabled", false);
        check("mysql.host", "host");
        check("mysql.port", 3306);
        check("mysql.database", "database");
        check("mysql.username", "username");
        check("mysql.password", "password");
        getServer().getPluginManager().registerEvents(new PointsListener(), this);
        getCommand("heliospoints").setExecutor(new HeliosPointsCommand());
        if (getConfig().getBoolean("mysql.enabled")) {
            mysql = new MySQL(getConfig().getString("mysql.host"), getConfig().getInt("mysql.port"),
                    getConfig().getString("mysql.database"), getConfig().getString("mysql.username"),
                    getConfig().getString("mysql.password"));
            if (mysql.getConnection() != null) {
                databaseType = DatabaseType.MYSQL;
                System.out.println("[HeliosPointsAPI] DatabaseType is mysql.");
            } else {
                databaseType = DatabaseType.CONFIG;
                System.out.println("[HeliosPointsAPI] DatabaseType is config.");
            }
        } else {
            databaseType = DatabaseType.CONFIG;
            System.out.println("[HeliosPointsAPI] DatabaseType is config.");
        }
        Bukkit.getMessenger().registerIncomingPluginChannel(this, "HeliosBungee", new MessageReciveListener());
        for (Player player : Bukkit.getOnlinePlayers())
            PlayerManager.loadPlayer(player);
    }

    @Override
    public void onDisable() {
        for (Player player : Bukkit.getOnlinePlayers())
            PlayerManager.unloadPlayer(player);
        if (mysql != null)
            mysql.close();
    }

    private void check(String path, Object value) {
        if (!getConfig().contains(path)) {
            getConfig().set(path, value);
            saveConfig();
        }
    }

    public static Main getInstance() {
        return instance;
    }

    public MySQL getMysql() {
        return mysql;
    }

    public DatabaseType getDatabaseType() {
        return databaseType;
    }

}
