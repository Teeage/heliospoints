package de.heliosdevelopment.heliospoints.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.UUID;

public class PointsChangeEvent extends Event {

	private static final HandlerList handlers = new HandlerList();
	private final UUID uuid;
	private final int oldPoints;
	private final int newPoints;

	public PointsChangeEvent(UUID uuid, int oldPoints, int newPoints) {
		this.uuid = uuid;
		this.oldPoints = oldPoints;
		this.newPoints = newPoints;
	}

	public UUID getUuid() {
		return this.uuid;
	}

	public int getOldPoints() {
		return oldPoints;
	}

	public int getNewPoints() {
		return newPoints;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

}
