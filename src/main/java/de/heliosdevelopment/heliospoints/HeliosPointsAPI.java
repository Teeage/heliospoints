package de.heliosdevelopment.heliospoints;

import java.io.File;
import java.util.UUID;

import de.heliosdevelopment.heliospoints.events.PointsChangeEvent;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;

import de.heliosdevelopment.heliospoints.player.PlayerManager;
import de.heliosdevelopment.heliospoints.player.PointsPlayer;

public class HeliosPointsAPI {

    private final static Main plugin = Main.getInstance();
    private final static File file = new File(plugin.getDataFolder() + "//data.yml");
    private final static YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);

    /**
     * Get points from a player
     *
     * @param uuid of the player
     * @return The current number of points
     */
    public static int getPoints(UUID uuid) {
        if (uuid == null)
            return 0;
        PointsPlayer pointsPlayer = PlayerManager.getPlayer(uuid);
        if (pointsPlayer != null) {
            if (pointsPlayer.getPoints() == -1)
                return 0;
            return pointsPlayer.getPoints();
        } else {
            if (plugin.getDatabaseType() == DatabaseType.MYSQL) {
                int points = plugin.getMysql().getInt(uuid.toString());
                if (points == -1)
                    return 0;
                return points;
            } else {
                if (cfg.contains(uuid.toString())) {
                    int points = cfg.getInt(uuid.toString());
                    if (points == -1)
                        return 0;
                    return points;
                }
            }
        }
        return 0;

    }

    /**
     * Add the defined player the defined amount of points
     *
     * @param uuid   The player who get the points
     * @param amount The amount of Points
     */
    public static void addPoints(UUID uuid, int amount) {
        if (uuid == null || amount == 0)
            return;
        PointsPlayer pointsPlayer = PlayerManager.getPlayer(uuid);
        if (pointsPlayer != null) {
            int oldPoints = pointsPlayer.getPoints();
            int newPoints = oldPoints + amount;
            pointsPlayer.setPoints(newPoints);
            Bukkit.getPluginManager().callEvent(new PointsChangeEvent(uuid, oldPoints, newPoints));
        } else {
            if (plugin.getDatabaseType() == DatabaseType.MYSQL) {
                plugin.getMysql().setPoints(uuid.toString(), getPoints(uuid) + amount);
            } else {
                if (cfg.contains(uuid.toString()))
                    cfg.set(uuid.toString(), getPoints(uuid) + amount);
                else
                    cfg.set(uuid.toString(), amount);
            }
        }
    }

    /**
     * Remove points from the define player
     *
     * @param uuid   The player who lost the points
     * @param amount The amount of Points
     */
    public static boolean removePoints(UUID uuid, Integer amount) {
        if (uuid == null || amount == 0)
            return false;
        if (getPoints(uuid) < amount)
            return false;
        PointsPlayer pointsPlayer = PlayerManager.getPlayer(uuid);
        if (pointsPlayer != null) {
            int oldPoints = pointsPlayer.getPoints();
            int newPoints = oldPoints - amount;
            pointsPlayer.setPoints(newPoints);
            Bukkit.getPluginManager().callEvent(new PointsChangeEvent(uuid, oldPoints, newPoints));
        } else {
            if (plugin.getDatabaseType() == DatabaseType.MYSQL) {
                plugin.getMysql().setPoints(uuid.toString(), getPoints(uuid) - amount);
            } else {
                if (cfg.contains(uuid.toString()))
                    cfg.set(uuid.toString(), getPoints(uuid) - amount);
                else
                    cfg.set(uuid.toString(), amount);
            }
        }
        return true;
    }

    /**
     * Set the points from a player to the define amount
     *
     * @param uuid   The Player
     * @param amount The amount of Points
     */
    public static void setPoints(UUID uuid, Integer amount) {
        if (uuid == null || amount == 0)
            return;
        PointsPlayer pointsPlayer = PlayerManager.getPlayer(uuid);
        if (pointsPlayer != null) {
            int oldPoints = pointsPlayer.getPoints();
            pointsPlayer.setPoints(amount);
            Bukkit.getPluginManager().callEvent(new PointsChangeEvent(uuid, oldPoints, amount));
        } else {
            if (plugin.getDatabaseType() == DatabaseType.MYSQL) {
                plugin.getMysql().setPoints(uuid.toString(), amount);
            } else {
                cfg.set(uuid.toString(), amount);
            }
        }
    }

    public static File getFile() {
        return file;
    }

    public static YamlConfiguration getConfiguration() {
        return cfg;
    }

}
