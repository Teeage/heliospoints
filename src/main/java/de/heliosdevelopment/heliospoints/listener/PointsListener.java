package de.heliosdevelopment.heliospoints.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import de.heliosdevelopment.heliospoints.player.PlayerManager;

public class PointsListener implements Listener {
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		PlayerManager.loadPlayer(event.getPlayer());
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		PlayerManager.unloadPlayer(event.getPlayer());
	}

}
