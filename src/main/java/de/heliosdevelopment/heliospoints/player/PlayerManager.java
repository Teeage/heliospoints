package de.heliosdevelopment.heliospoints.player;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import de.heliosdevelopment.heliospoints.DatabaseType;
import de.heliosdevelopment.heliospoints.HeliosPointsAPI;
import de.heliosdevelopment.heliospoints.Main;
import org.bukkit.entity.Player;

public class PlayerManager {

	private final static Set<PointsPlayer> players = new HashSet<>();
	private final static Main plugin = Main.getInstance();

	public static void loadPlayer(Player player) {
		int points;
		if (plugin.getDatabaseType() == DatabaseType.MYSQL) {
			points = plugin.getMysql().getInt(player.getUniqueId().toString());
		} else {
			points = HeliosPointsAPI.getConfiguration().getInt(player.getUniqueId().toString());
		}
		players.add(new PointsPlayer(player.getUniqueId(), points));
	}

	public static void unloadPlayer(Player player) {
		PointsPlayer pointsPlayer = getPlayer(player.getUniqueId());
		if (pointsPlayer != null) {
			if (plugin.getDatabaseType() == DatabaseType.MYSQL) {
				plugin.getMysql().setPoints(pointsPlayer.getUuid().toString(), pointsPlayer.getPoints());
			} else {
				HeliosPointsAPI.getConfiguration().set(pointsPlayer.getUuid().toString(), pointsPlayer.getPoints());
				try {
					HeliosPointsAPI.getConfiguration().save(HeliosPointsAPI.getFile());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static PointsPlayer getPlayer(UUID uuid) {
		for (PointsPlayer pointsPlayer : players)
			if (pointsPlayer.getUuid().equals(uuid))
				return pointsPlayer;
		return null;

	}

}
